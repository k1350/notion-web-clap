function doGet(e) {
  const params = e.parameters;
  if (params["url"] == null) {
    return buildResponse(false);
  }
  if (!isAllowedUrl(e.parameter.url)) {
    return buildResponse(false);
  }
  return clap(e.parameter.url);
}

/**
 * 指定の URL に対して「拍手」する
 * @param {String} url 拍手対象の URL
 * @return {Boolean} 正常に「拍手」できたら true、できなかったら false
 */
function clap(url) {
  const scriptProperties = PropertiesService.getScriptProperties();
  const database_id = scriptProperties.getProperty('database_id');

  // 対象URLのレコードが存在するかどうか確認する
  const read_api_url = 'databases/' + database_id + '/query';
  const read_filter = {
    "filter": {
      "property": "url",
      "text": {
        "equals": url
      }
    }
  };

  const read_res = curlNotion('post', read_api_url, read_filter);

  if (read_res == false) {
    return buildResponse(false);
  }

  if (read_res.results.length == 0) {
    // 既存データが無いので新規投稿
    const post_data = {
      'parent': { 'database_id': database_id },
      'properties': {
        'url': {
          'title': [
            {
              'text': {
                'content': url,
              }
            }
          ]
        },
        'count': {
          'type': 'number',
          'number': 1
        }
      }
    };
    const post_res = curlNotion('post', 'pages', post_data);
    if (post_res == false) {
      return buildResponse(false);
    }
    return buildResponse(true);
  }
  // 既存データのカウント＋1
  const page_id = read_res.results[0].id;
  const count = read_res.results[0].properties.count.number + 1;
  const patch_data = {
    "properties": {
      "count": { "number": count }
    }
  };
  const patch_res = curlNotion('patch', 'pages/' + page_id, patch_data);
  if (patch_res == false) {
    return buildResponse(false);
  }
  return buildResponse(true);
}

/**
 * Notion の API を叩く
 * @param {String} method HTTPメソッド。get / post / patch
 * @param {String} url エンドポイントの URL
 * @param {Object} data リクエストボディの JSON データ
 * @return {Object|Boolean} 正常にリクエストが完了したらレスポンスデータ、完了しなかったら false
 */
function curlNotion(method, url, data) {
  const scriptProperties = PropertiesService.getScriptProperties();
  const notion_token = scriptProperties.getProperty('token');
  const headers = {
    'Content-Type': 'application/json; charset=UTF-8',
    'Authorization': 'Bearer ' + notion_token,
    'Notion-Version': '2021-08-16',
  };
  const options = {
    "method": method,
    "headers": headers,
    "payload": JSON.stringify(data)
  };

  try {
    const res = UrlFetchApp.fetch('https://api.notion.com/v1/' + url, options);
    return JSON.parse(res.getContentText());
  } catch (e) {
    console.log(e);
    return false;
  }
}

/**
 * 拍手結果のレスポンスを生成する
 * @param {Boolean} result 正常に拍手できたら true、できなかったら false
 * @return {Object} 拍手結果のレスポンスデータ
 */
function buildResponse(result) {
  var output = ContentService.createTextOutput();
  output.setMimeType(ContentService.MimeType.JSON);
  const content = {
    "result": result
  }
  output.setContent(JSON.stringify(content));
  return output;
}

/**
 * 拍手対象として許可されている URL かどうか判定する
 * @param {String} url 拍手対象の URL
 * @return {Boolean} 許可されているなら true、されていないなら false
 */
function isAllowedUrl(url) {
  const scriptProperties = PropertiesService.getScriptProperties();
  const allow_url = scriptProperties.getProperty('allow_url');
  const reg = new RegExp("^" + allow_url);
  const result = url.match(reg);
  if (result == null) {
    return false;
  }
  return true;
}