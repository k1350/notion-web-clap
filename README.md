# Notion Web Clap

Notion API と Google Apps Script で作ったweb拍手みたいな物

Notion-Version: 2021-08-16

## Getting started

(1) Notion に データベースを作成する

データベースに必要なのは下記のプロパティ。

- url: プロパティの種類…タイトル
- count: プロパティの種類…数値

(2) Notion API が使えるようにインテグレーションの追加等を済ませる

(3) Google Apps Script のプロパティに下記の値を設定する

- token: Notion API キー
- database_id: 手順 (1) で作ったデータベースの ID
- allow_url: 拍手対象として許可する URL（拍手設置サイトの URL）
    * allow_url に設定した URL から始まる URL を拍手対象として許可します
    * 例： allow_url に https://example.com と設定すると、https://example.com/test なども許可されます

(4) Google Apps Script に main.gs の中身を張り付けてウェブアプリとしてデプロイする

「次のユーザーとして実行」は「自分」、「アクセスできるユーザー」は「全員」を選択する。

(5) デプロイ後、ウェブアプリの URL に ?url=https://example.com のように拍手対象の URL をクエリストリングとしてつけて Get アクセスすると拍手回数が Notion のデータベースに記録される

## License

誰でも無償で無制限に扱って構いません。著作権表記も行う必要はありません。

作者は、本ソースコードに関して一切の責任を負いません。